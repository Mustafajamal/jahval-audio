<?php
/**
 * Plugin Name:       Quote Generator
 * Plugin URI:        logicsbuffer.com/
 * Description:       Custom Quotation generator for LED Panels [quote_generator]
 * Version:           1.0.0
 * Requires at least: 5.2
 * Requires PHP:      7.2
 * Author:            Mustafa jamal
 * Author URI:        logicsbuffer.com/
 * License:           GPL v2 or later
 * License URI:       https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain:       quote-generator
 * Domain Path:       /languages
 */


add_action( 'init', 'custom_fancy_product' );

function custom_fancy_product() {

	//add_shortcode( 'show_custom_fancy_product', 'custom_fancy_product_form' );
	add_shortcode( 'quote_generator', 'custom_fancy_product_form_single' );
	add_action( 'wp_enqueue_scripts', 'custom_fancy_product_script' );
	//add_action( 'wp_ajax_nopriv_post_love_calculateQuote', 'post_love_calculateQuote' );
	//add_action( 'wp_ajax_post_love_calculateQuote', 'post_love_calculateQuote' );
	// Setup Ajax action hook
	add_action( 'wp_ajax_read_me_later', array( time(), 'read_me_later' ) );
	add_action( 'wp_ajax_nopriv_read_me_later', array( time(), 'read_me_later' ) );

}



 function custom_fancy_product_script() {
		        
	wp_enqueue_script( 'rt_custom_fancy_script', plugins_url().'/custom_fancy_product/js/custom_fancy1.js',array(),time());	
	wp_enqueue_script( 'rt_masking_script', plugins_url().'/custom_fancy_product/js/masking.js');
	wp_enqueue_style( 'rt_custom_fancy_style', plugins_url().'/custom_fancy_product/css/custom_fancy.css');

	if( is_page( 'custom-banner')){
	wp_enqueue_style( 'rt_fancy_bootstrap', plugins_url().'/custom_fancy_product/css/bootstrap1.css');
	}
}

// add_filter( 'woocommerce_get_price_html', 'bbloomer_alter_price_display', 9999, 2 );
 
// function bbloomer_alter_price_display( $price_html, $product ) {
    
//     // ONLY IF PRICE NOT NULL
//     //if ( '' === $product->get_price() ) return $price_html;
//     global $product;
// 	$product_id = $product->get_id();

//     $total_price = get_post_meta('total_price_quote');
//     $orig_price = wc_get_price_to_display( $product_id );
//     $price_html = wc_price( $orig_price + $total_price );
        
//     return $price_html;
 
// }
// function return_custom_price($price, $product) {

//     global $product;
//     global $current_user;

//     $myPrice = get_post_meta('total_price_quote');
//     $price = $myPrice;
//     echo $price;
//     echo $price;
//     echo $price;
//     //$post_id = $product->get_id();
//     return $price;
// }

//Test

//add_action( 'woocommerce_before_calculate_totals', 'add_custom_price' );

// add_action( 'woocommerce_before_calculate_totals', 'add_custom_price' );
// function add_custom_price( $cart_object ) {
// 	global $post;
// 	$product_id = $post->ID;    
//     $myPrice = get_post_meta('total_price_quote');

//     // Get the WC_Product Object instance
// 	$product = wc_get_product( $product_id );

// 	// Set the product active price (regular)
// 	$product->set_price( $myPrice );
// 	$product->set_regular_price( $myPrice ); // To be sure

// 	// Save product data (sync data and refresh caches)
// 	$product->save();

// }
//Test
// function woocommerce_custom_price_to_cart_item( $cart_object ) {  
//     if( !WC()->session->__isset( "reload_checkout" )) {
//         foreach ( $cart_object->cart_contents as $key => $value ) {
//             if( isset( $value["custom_price"] ) ) {
//                 //for woocommerce version lower than 3
//                 //$value['data']->price = $value["custom_price"];
//                 //for woocommerce version +3
//                 $value['data']->set_price($value["custom_price"]);
//             }
//         }  
//     }  
// }
// add_action( 'woocommerce_before_calculate_totals', 'woocommerce_custom_price_to_cart_item', 99 );

function read_me_later(){
			

			$total_price = $_REQUEST['total_price'];
			update_post_meta('total_price_quote', $total_price);
			
			// global $post;
			// global $woocommerce;

			// $product_id = $post->ID;
			// echo $product_id;
			// WC()->cart->add_to_cart($product_id);
// Simple, grouped and external products
// add_filter('woocommerce_product_get_price', array( $this, 'custom_price' ), 99, 2 );
// add_filter('woocommerce_product_get_regular_price', array( $this, 'custom_price' ), 99, 2 );
			
			//add_custom_price();
			// $rand = rand(1, 9000);
			// // Insert the post into the database
			// $can_height = $_REQUEST['can_height'];
			// $can_width = $_REQUEST['can_width'];
			// $size_unit = $_REQUEST['size_unit'];
			// $banner_qty = $_REQUEST['banner_qty'];
			// $banner_material = $_REQUEST['banner_material'];
			// $banner_finishing = $_REQUEST['banner_finishing'];
			// $banner_delivery = $_REQUEST['banner_delivery'];
			// $per_banner_price = $_REQUEST['per_banner_price'];
			// $height = $_REQUEST['cheight'];
			// $width = $_REQUEST['cwidth'];
			// $inch_height = $_REQUEST['inch_height'];
			// $inch_width = $_REQUEST['inch_width'];
			// $product_title ="<p>Material: ".ucfirst($banner_material).'</br>'.$can_width.' '.ucfirst($size_unit).' &#10005; '.$can_height.' '.ucfirst($size_unit).'</br>Finishing: '.ucfirst($banner_finishing).'</br>Delivery: '.ucfirst($banner_delivery).'</br>Quantity: '.ucfirst($banner_qty)."</p>";
			// if($inch_height){
			// 	$can_height = floor($can_height).' foot-'.$inch_height.' inch';
				
			// }
			// if($inch_width){
			// 	$can_width = floor($can_width).' foot-'.$inch_width.' inch';
			// }
			// if($inch_width || $inch_height){
			// $product_title ="<p>Material: ".ucfirst($banner_material).'</br>'.floor($can_width).' '.ucfirst($size_unit).' '.$inch_width.' inch'.' &#10005; '.floor($can_height).' '.ucfirst($size_unit).' '.$inch_height.' inch'.'</br>Finishing: '.ucfirst($banner_finishing).'</br>Deliverffy: '.ucfirst($banner_delivery).'</br>Quantity: '.ucfirst($banner_qty)."</p>";
			// }
			// $price = $_REQUEST['price'];
			// $my_post = array(
			//   'post_title'    => $product_title,
			//   'post_content'  => 'test_post',
			//   'post_type' => 'product',
			//   'post_status'   => 'publish'
			// );
			// $product_id = wp_insert_post( $my_post );
			// $canvas_size = '{\\&quot;stage_width\\&quot;:\\&quot;'.$width.'\\&quot;,\\&quot;stage_height\\&quot;:\\&quot;'.$height.'\\&quot;}';
			// //$fancy_product_id = 1;
			// //get_product_html( $product_id );
			// 	global $wpdb;
			// 	$table_name = $wpdb->prefix . 'fpd_products';
			// 	$wpdb->insert( 
			// 		$table_name, 
			// 		array( 
			// 			'ID' => $product_id, 
			// 			'title' => $product_title,
			// 			'options' => $canvas_size,
			// 			'thumbnail' => 'http://designtool.efound.co.uk/wp-content/uploads/2016/11/default_image_01.png' 
			// 		) 
			// 	);
			// 	$table_name = $wpdb->prefix . 'fpd_views';
			// 	$wpdb->insert( 
			// 		$table_name, 
			// 		array( 
			// 			'ID' => '', 
			// 			'product_id' => $product_id,
			// 			'title' => 'front',
			// 			'thumbnail' => 'http://designtool.efound.co.uk/wp-content/uploads/2016/11/default_image_01.png',
			// 			'elements' => '', 
			// 			'view_order' => '0',
			// 			'options' => '' 
			// 		) 
			// 	);
			// 	update_post_meta($product_id, 'fpd_source_type', 'product'); 
			// 	update_post_meta($product_id, 'fpd_products', $product_id);
			// 	update_post_meta($product_id, '_price', $price);
			// 	update_post_meta($product_id, '_regular_price', $price);
			// 	update_post_meta($product_id, 'width_wdith', $can_width);
			// 	update_post_meta($product_id, 'width_length',  $can_height);
			// 	update_post_meta($product_id, 'width_unit',    $size_unit);			
			// 	update_post_meta($product_id, 'width_qty', $banner_qty);
			// 	update_post_meta($product_id, 'width_delivery', $banner_delivery);
			// 	update_post_meta($product_id, 'width_finishing', $banner_finishing);
			// 	update_post_meta($product_id, 'width_material', $banner_material);
			// 	update_post_meta($product_id, 'per_banner_price', $per_banner_price);
			// 	$url = get_permalink($product_id);
			// 	print_r($url);
			// 	die();

	
}


function custom_fancy_product_form_single() {
				$plugins_url =  plugins_url( 'images/icons/', __FILE__ );
				global $post;
				global $woocommerce;
				$product_id = $post->ID;

				if(isset($_POST['submit_prod'])){
						
					//$total_qty = $_POST['rt_qty'];			 
					$quantity = 10;			 
					// //update_post_meta($new_post_id, 'width_finishing', $rt_finishing);
					$rt_total_price = $_POST['pricetotal_quote'];
					
					echo $rt_total_price;
					echo $product_id;
					
					//Set price
					global $post;
					$product_id = $post->ID;    
				    //$myPrice = get_post_meta('total_price_quote');
				    $myPrice = $rt_total_price;

				    // Get the WC_Product Object instance
					$product = wc_get_product( $product_id );

					// Set the product active price (regular)
					$product->set_price( $myPrice );
					$product->set_regular_price( $myPrice ); // To be sure

					// Save product data (sync data and refresh caches)
					$product->save();
					//Set price end

					//die();
					$custom_price = $rt_total_price;
					// Cart item data to send & save in order
					 $cart_item_data = array('custom_price' => $custom_price);   
					// woocommerce function to add product into cart check its documentation also 
					// what we need here is only $product_id & $cart_item_data other can be default.
					WC()->cart->add_to_cart( $product_id, $quantity, $variation_id, $variation, $cart_item_data);
					// // Calculate totals
					WC()->cart->calculate_totals();
					// // Save cart to session
					WC()->cart->set_session();
					// // Maybe set cart cookies
					WC()->cart->maybe_set_cart_cookies();
				
				}
				ob_start();
				//$page_title = get_the_title();
				//$terms = get_the_terms( get_the_ID(), 'product_cat' );
				//$product_type = $terms[0]->slug;
				?>
<script type="text/javascript">
    var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
</script>				
<div class="custom_calculator single_product">
	<h3 class="calculator_title">Price Calculator</h3>
	<div class="orderform" id="form1">
	
		<form role="form" action="" method="post" id="add_cart" enctype="multipart/form-data" method="post">						
					    						
				<!-- <span id="price"/></span> -->
				<input type="hidden" id="eprice" />
				<input type="hidden" id="hprice" />						
				<input type="hidden" id="psqft" />						
				<input type="hidden" id="efinal" />
				<input type="hidden" id="qfinal" />
				<input type="hidden" id="sqft_total" />
				<input type="hidden" id="eyeletsT1" />
				<input type="hidden" id="hammingT1" />
				<input type="hidden" id="deliveryT1" />

			
			<div class="row widthheight_main">
				<div class="col-sm-4 wh_label">
					<label class="control-label" for="width">Width:</label>
					<input onKeyup="calculateQuote()" placeholder="0.00"  type="text" name="rt_width" class="form-control" id="rt_width" value="4" required>	
				</div>
			
				<div class="col-sm-4 wh_label">
					<label class="control-label"  for="height">Height:</label>
					<input value="2" onKeyup="calculateQuote()"  placeholder="0.00" type="text" name="rt_height" class="form-control" id="rt_height" required>	
				</div>
			</div>
			<!--<div class="row calculations_main">
				<div class="col-sm-3"><strong>Panel Cost:</strong> <span id="panel_cost_total"></span></div>
				<div class="col-sm-3"><strong>Bracket Cost:</strong> <span id="bracket_cost_total"></span></div>
				<div class="col-sm-3"><strong>Case Cost:</strong> <span id="case_cost_total"></span></div>
				<div class="col-sm-3"><strong>Processor Cost:</strong> <span id="processor_cost_total"></span></div>
			</div>-->
			

			<!-- Divi theme layout -->
			<div class="et_pb_row et_pb_row_10_tb_body et_pb_gutters2">
				<div class="et_pb_with_border et_pb_column_1_2 et_pb_column et_pb_column_14_tb_body product-center  et_pb_css_mix_blend_mode_passthrough">
				
				<div class="et_pb_module et_pb_text et_pb_text_11_tb_body  et_pb_text_align_right et_pb_bg_layout_light">
				
				<div class="et_pb_text_inner"><p>Panel Cost<br> Bracket Cost<br> Case Cost<br> Processor Cost</p></div>
				</div> <!-- .et_pb_text -->
				</div> <!-- .et_pb_column --><div class="et_pb_with_border et_pb_column_1_2 et_pb_column et_pb_column_15_tb_body product-center  et_pb_css_mix_blend_mode_passthrough et-last-child">
										
					<div class="et_pb_with_border et_pb_module et_pb_text et_pb_text_12_tb_body  et_pb_text_align_left et_pb_bg_layout_light">
					<div class="et_pb_text_inner"><p><span id="panel_cost_total"></span><br> <span id="bracket_cost_total"></span><br> <span id="case_cost_total"></span><br> <span id="processor_cost_total"></span></p></div>
				</div> <!-- .et_pb_text -->
				</div> <!-- .et_pb_column -->
					
				
			</div>

			<div class="et_pb_section et_pb_section_7_tb_body et_section_regular">
				
			<div class="et_pb_row et_pb_row_11_tb_body">
				<div class="et_pb_with_border et_pb_column_4_4 et_pb_column et_pb_column_16_tb_body product-center  et_pb_css_mix_blend_mode_passthrough et-last-child">				
				<div class="et_pb_module et_pb_wc_price et_pb_wc_price_0_tb_body  et_pb_text_align_center">
				<div class="et_pb_module_inner">
					<p class="price"><span class="woocommerce-Price-amount amount"><bdi><span class="woocommerce-Price-currencySymbol">$</span><span class="price_total">0.00</span> </bdi></span></p>

				</div>
				</div>
				</div> <!-- .et_pb_column -->
				
			</div> <!-- .et_pb_row -->
			</div>
			<!-- Divi theme layout Ends-->

			<div style="display:none;" class="sqft_oneway">	
				<div class="form-group">
					<input placeholder="0"  type="hidden" name="sqft" class="form-control" id="sqft" readonly>									
					<input placeholder="0"  type="hidden" name="sqfttotal" class="form-control" id="sqfttotal" readonly>									
				</div>
			</div>
			
			<div class="row total_row_main">	
				<div class="col-sm-12">
					<div class="pricetotal_quote" style="display: none;">				
						<input placeholder="0.00" type="text" value="13.60" name="pricetotal_quote" class="form-control" id="pricetotal_quote" readonly=""> 	
						<div id="total_cart_main"></div>
					</div>				
						<input style="display:none;" placeholder="0" type="text" name="price_per_banner" value="9.4" class="form-control" id="price_per_banner" readonly="">
						<input style="display:none;" placeholder="0" type="text" name="price" class="form-control" id="price" readonly="">
				</div>	
			</div>
			<div class="row add_cart_row_main">
				<div class="col-sm-12">
					<input type="submit" name="submit_prod" value="Add to Cart" class="btn-addcart" id="btn_addcart_custom">
				</div>		
			</div>
		
	</form>
	</div>
</div>
	
<?php 
return ob_get_clean();
}

add_action('woocommerce_add_order_item_meta','order_meta_handler', 1, 3);

add_filter('woocommerce_add_cart_item_data', 'add_cart_item_custom_data', 10, 2);
function order_meta_handler($item_id, $values, $cart_item_key) {
	// Allow plugins to add order item meta
			/* ?><pre><?php print_r($cart_session); ?></pre><?php */

			$cart_session = WC()->session->get('cart');
			if($cart_session[$cart_item_key]['rt_material'][0]){
			wc_add_order_item_meta($item_id, "Material", $cart_session[$cart_item_key]['rt_material'][0]);
			}
			if($cart_session[$cart_item_key]['rt_unit'][0]){
			wc_add_order_item_meta($item_id, "Unit", $cart_session[$cart_item_key]['rt_unit'][0]);
			}
			if($cart_session[$cart_item_key]['rt_width'][0]){
			wc_add_order_item_meta($item_id, "Width", $cart_session[$cart_item_key]['rt_width'][0]);
			}
			if($cart_session[$cart_item_key]['rt_height'][0]){
			wc_add_order_item_meta($item_id, "Height", $cart_session[$cart_item_key]['rt_height'][0]);
			}
			if($cart_session[$cart_item_key]['rt_finishing'][0]){
			wc_add_order_item_meta($item_id, "Finishing", $cart_session[$cart_item_key]['rt_finishing'][0]);
			}
			if($cart_session[$cart_item_key]['rt_double_sided'][0]){
			wc_add_order_item_meta($item_id, "Double Sided", $cart_session[$cart_item_key]['rt_double_sided'][0]);
			}
			if($cart_session[$cart_item_key]['rt_delivery'][0]){
			wc_add_order_item_meta($item_id, "Delivery", $cart_session[$cart_item_key]['rt_delivery'][0]);
			}
			if($cart_session[$cart_item_key]['rt_qty'][0]){
			wc_add_order_item_meta($item_id, "Quantity", $cart_session[$cart_item_key]['rt_qty'][0]);
			}
			if($cart_session[$cart_item_key]['upload_artwork_url'][0]){
			wc_add_order_item_meta($item_id, "Artwork url", $cart_session[$cart_item_key]['upload_artwork_url'][0]);
			}
			if($cart_session[$cart_item_key]['rt_artwork_url2'][0]){
			wc_add_order_item_meta($item_id, "Artwork url 2", $cart_session[$cart_item_key]['rt_artwork_url2'][0]);
			}
			if($cart_session[$cart_item_key]['rt_artwork_url3'][0]){
			wc_add_order_item_meta($item_id, "Artwork url 3", $cart_session[$cart_item_key]['rt_artwork_url3'][0]);
			}
			if($cart_session[$cart_item_key]['rt_artwork_url4'][0]){
			wc_add_order_item_meta($item_id, "Artwork url 4", $cart_session[$cart_item_key]['rt_artwork_url4'][0]);
			}
			if($cart_session[$cart_item_key]['rt_artwork_url5'][0]){
			wc_add_order_item_meta($item_id, "Artwork url 5", $cart_session[$cart_item_key]['rt_artwork_url5'][0]);
			}
			if($cart_session[$cart_item_key]['rt_banner_artwork_urls'][0]){
			wc_add_order_item_meta($item_id, "Artwork Files", $cart_session[$cart_item_key]['rt_banner_artwork_urls'][0]);
			}
			if($cart_session[$cart_item_key]['rt_line1_text'][0]){
			wc_add_order_item_meta($item_id, "Line 1 Text", $cart_session[$cart_item_key]['rt_line1_text'][0]);
			}
			if($cart_session[$cart_item_key]['rt_line2_text'][0]){
			wc_add_order_item_meta($item_id, "Line 2 Text", $cart_session[$cart_item_key]['rt_line2_text'][0]);
			}
			if($cart_session[$cart_item_key]['rt_banner_inst'][0]){
			wc_add_order_item_meta($item_id, "Banner Instruction", $cart_session[$cart_item_key]['rt_banner_inst'][0]);
			}
			
		}
		function add_cart_item_custom_data($cart_item_meta, $new_post_id) {
			global $woocommerce;
			$rt_price = get_post_meta($new_post_id, 'width_price');
			$rt_fpd_products = get_post_meta($new_post_id, 'fpd_products');
			$rt_width = get_post_meta($new_post_id, 'width_wdith');
			$rt_height = get_post_meta($new_post_id, 'width_length');
			$upload_artwork = get_post_meta($new_post_id, 'upload_artwork');
			$rt_unit = get_post_meta($new_post_id, 'width_unit');
			$rt_qty = get_post_meta($new_post_id, 'width_qty');
			$rt_delivery = get_post_meta($new_post_id, 'width_delivery');
			$rt_order_refrence_number = get_post_meta($new_post_id, 'width_order_refrence_number');
			$rt_finishing = get_post_meta($new_post_id, 'width_finishing');
			$rt_double_sided = get_post_meta($new_post_id, 'width_double_sided');
			$rt_material = get_post_meta($new_post_id, 'width_material');
			$per_banner_price = get_post_meta($new_post_id, 'per_banner_price');
			$can_background_color = get_post_meta($new_post_id, 'can_background_color');
			$artwork_url_1 = get_post_meta($new_post_id, 'artwork_url_0');
			$artwork_url_2 = get_post_meta($new_post_id, 'artwork_url_1');
			$artwork_url_3 = get_post_meta($new_post_id, 'artwork_url_2');
			$artwork_url_4 = get_post_meta($new_post_id, 'artwork_url_3');
			$artwork_url_5 = get_post_meta($new_post_id, 'artwork_url_4');
			$line1_text = get_post_meta($new_post_id, 'line1_text');
			$line2_text = get_post_meta($new_post_id, 'line2_text');
			$banner_inst = get_post_meta($new_post_id, 'banner_inst');
			$banner_artwork_urls = get_post_meta($new_post_id, 'banner_artwork_urls');
			$custom_birthday_qty = get_post_meta($new_post_id, 'custom_birthday_qty');
            $cart_item_meta['rt_fpd_products'] = $rt_fpd_products;
            $cart_item_meta['rt_width'] = $rt_width;
            $cart_item_meta['rt_height'] = $rt_height;
            $cart_item_meta['upload_artwork_url'] = $upload_artwork;
            $cart_item_meta['rt_unit'] = $rt_unit;
            $cart_item_meta['rt_qty'] = $rt_qty;
            $cart_item_meta['rt_delivery'] = $rt_delivery;
            $cart_item_meta['rt_order_refrence_number'] = $rt_order_refrence_number;
            $cart_item_meta['rt_double_sided'] = $rt_double_sided;
            $cart_item_meta['rt_finishing'] = $rt_finishing;
            $cart_item_meta['rt_material'] = $rt_material;
            $cart_item_meta['per_banner_price'] = $per_banner_price;
            $cart_item_meta['can_background_color'] = $can_background_color;
            $cart_item_meta['rt_artwork_url1'] = $artwork_url_1;
            $cart_item_meta['rt_artwork_url2'] = $artwork_url_2;
            $cart_item_meta['rt_artwork_url3'] = $artwork_url_3;
            $cart_item_meta['rt_artwork_url4'] = $artwork_url_4;
            $cart_item_meta['rt_artwork_url5'] = $artwork_url_5;
            $cart_item_meta['rt_banner_artwork_urls'] = $banner_artwork_urls;
            $cart_item_meta['rt_line1_text'] = $line1_text;
            $cart_item_meta['rt_line2_text'] = $line2_text;
            $cart_item_meta['rt_banner_inst'] = $banner_inst;
            $cart_item_meta['custom_birthday_qty'] = $custom_birthday_qty;
            return $cart_item_meta;
		}

/*
add_action( 'woocommerce_before_add_to_cart_form', 'woocommerce_single_variation_add_to_cart_button', 20 );
function woocommerce_single_variation_add_to_cart_button(){
    echo do_shortcode('[quote_generator]');
}*/