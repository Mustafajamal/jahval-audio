jQuery(document).ready(function() {


function set_product_canvas(){

}

});

	function calculateQuote(){
			
		//document.getElementById("pricetotal_quote").value = 8.00;
		var each_panel = 19.685;

		var width_feet = document.getElementById("rt_width").value;
		var height_feet = document.getElementById("rt_height").value;
		
		var width_inches = parseInt(width_feet) * parseInt(12);
		var height_inches = parseInt(height_feet) * parseInt(12);

		var panel_width =  parseInt(width_inches) / parseFloat(each_panel);
		var panel_height = parseInt(height_inches) / parseFloat(each_panel);

		var panel_width_ciel = Math.floor(panel_width);
		var panel_height_ciel = Math.floor(panel_height);

		var total_no_panels = parseFloat(panel_width_ciel) * parseFloat(panel_height_ciel);

		//Brackets
		var total_brackets = panel_width_ciel;

		//Cases
		//There is 8 panels per case
		var panels_per_case = 8;
		var total_case =  total_no_panels /  panels_per_case + 1;

		//Pixels
		//1 pixel = 192
		Pixel_val = 192;

		//Pixels w = 12(brakets) * 192 = 2304 pixels in width

		var pixels_w = panel_width_ciel * Pixel_val;
		var pixels_h = panel_height_ciel * Pixel_val;

		//This number will define which processor you need.
		var total_pixels = pixels_w * pixels_h;

		//Processors
		if (total_pixels <= 1769472) {
			total_processors = 1;
		}
		else if (total_pixels >= 1769472 && total_pixels >= 3769472) {
			total_processors = 2;
		}

		//Price
		
		//Panel cost
		//Each panel cost = 596$, the dealer price = 797$, the price MSRP = 1,119.99$
		//Multiply price MSRP = 1,119.99$ * Total Panel
		var each_panel_cost = 1119.99;
		var panel_price = total_no_panels * each_panel_cost;

		//Bracket Price
		//Each bracket price = $209.99
		var each_bracket_cost = 209.99;
		var bracket_price = total_brackets * each_bracket_cost;

		//Case Price
		//Each Case price = $178.99
		var each_case_cost = 178.99;
		var case_price = total_case * each_case_cost;

		//Processor Price
		//Each Case price = $178.99
		var each_processor_cost = 1749.99;
		var processor_price = total_processors * each_processor_cost;

		//Total Price
		var total_price = panel_price + bracket_price + case_price + processor_price;

		
		console.log("panel_width.."+panel_width);
		console.log("panel_height.."+panel_height);
		console.log("total_no_panels.."+total_no_panels);
		console.log("Brackets.."+total_brackets);
		console.log("total_case.."+total_case);
		console.log("pixels_w.."+pixels_w);
		console.log("pixels_h.."+pixels_h);
		console.log("Total Processors.."+total_processors);
		console.log("total_pixels.."+total_pixels);

		console.log("panel_price.."+panel_price);
		console.log("bracket_price.."+bracket_price);
		console.log("case_price.."+case_price);
		console.log("processor_price.."+processor_price);


		//Set Costs to view
		jQuery("#panel_cost_total").text(panel_price.toFixed(2));
		jQuery("#bracket_cost_total").text(bracket_price.toFixed(2));
		jQuery("#case_cost_total").text(case_price.toFixed(2));
		jQuery("#processor_cost_total").text(processor_price.toFixed(2));


		console.log("total_price.."+total_price);


		document.getElementById("pricetotal_quote").value = total_price;
		jQuery(".price_total").html(total_price.toFixed(2));
		
		jQuery.ajax({
			url : ajaxurl,
			type : 'POST',
			data: {
                action: 'read_me_later',
                width_feet : width_feet,
				height_feet : height_feet,
				total_price : total_price
            },
			success : function( response ) {
				//url = response;
				//jQuery("#rt_waiting").css({"display": "none"});
				console.log("total_price....Ajax.."+total_price);
				//window.open(url,"_self");
			},
			error: function(e) {
			    console.log(e);
			}
		});
		


		// if (card_quantity == 100) {
		// 	card_type_rate = 8;
		// }else if (card_quantity == 200) {
		// 	card_type_rate = 16;
		// }else if(card_quantity == 500) {
		// 	card_type_rate = 36;
		// }else if(card_quantity == 1000) {
		// 	card_type_rate = 65;
		// }

		//var fpriceqtyfloat = parseFloat(card_quantity) * parseFloat(card_type_rate);
		//var fpriceqtyfloat = parseFloat(card_type_rate);
		//document.getElementById("pricetotal_oneway").value = fpriceqtyfloat;	
	}